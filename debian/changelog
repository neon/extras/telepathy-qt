telepathy-qt (0.9.8+ds-5) UNRELEASED; urgency=medium

  [ Pino Toscano ]
  * Drop the handling of the symlink-to-directory change for the documentation
    directories, as it was done before Debian Buster.
  * Drop the migration from libtelepathy-qt5-dbg, no more needed after two
    Debian stable releases.
  * Use execute_after_dh_auto_clean to avoid invoking dh_auto_clean manually.
  * Bump Standards-Version to 4.6.0, no changes required.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13

 -- Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>  Thu, 23 Sep 2021 08:23:35 +0200

telepathy-qt (0.9.8+ds-4) unstable; urgency=medium

  * Team upload.
  * Update symbols files.

 -- Pino Toscano <pino@debian.org>  Mon, 03 Aug 2020 00:32:37 +0200

telepathy-qt (0.9.8+ds-3) unstable; urgency=medium

  * Team upload.
  * Add Rules-Requires-Root: no.
  * Bump Standards-Version to 4.5.0, no changes required.
  * Stop passing --fail-missing to dh_missing, as it is the default behaviour
    in debhelper compatibility 12+.
  * Update symbols files for GCC 10. (Closes: #957867)
  * Use https for all the telepathy.freedesktop.org links.
  * Update symbols file reference URL in README.source.

 -- Pino Toscano <pino@debian.org>  Sun, 02 Aug 2020 21:17:13 +0200

telepathy-qt (0.9.8+ds-2) unstable; urgency=medium

  * Team upload.
  * Remove build dependencies: libgstreamer-plugins-base1.0-dev, python3-dev,
    and qttools5-dev.
  * Change the python3 build dependency to python3:any, as Python is needed
    only as build tool.
  * Update lintian overrides.
  * Update symbols file from the logs of buildds.
  * Do not include /usr/share/dpkg/architecture.mk in rules anymore, as it is
    not used.

 -- Pino Toscano <pino@debian.org>  Sun, 24 Nov 2019 09:56:16 +0100

telepathy-qt (0.9.8+ds-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update Vcs-* fields.
  * Python 3 is used instead of Python 2, so replace python, and python-dev
    with python3, and python3-dev. (Closes: #938643)
  * Remove the unused cmake parameters -DLIB_SUFFIX="/$(DEB_HOST_MULTIARCH)",
    and -DDESIRED_QT_VERSION=5.
  * We do not run the tests (as flaky), so:
    - remove the python3-dbus, and libxml2-utils build dependencies
    - add the -DENABLE_TESTS=NO cmake parameter to stop building them
  * The API documentation is not built, so:
    - remove the -DHAVE_DOT=YES cmake parameter
    - drop the improve-doxygen-build.patch patch
  * Add the -DENABLE_EXAMPLES=NO cmake parameter to disable the building of the
    examples, as they are not shipped.
  * Update symbols files.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Since we are repacking the tarball (to remove the pre-generated
    documentation):
    - remove the upstream signing key
    - add the +ds suffix to the version
    - adapt the watch file for the +ds suffix, and the lack of signature
  * Add the configuration for the CI on salsa.

 -- Pino Toscano <pino@debian.org>  Tue, 19 Nov 2019 11:23:08 +0100

telepathy-qt (0.9.7-5) unstable; urgency=medium

  * Team upload.
  * Update symbols file. (Closes: #897118)
  * Bump Standards-Version to 4.1.4, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 28 Apr 2018 17:44:10 +0200

telepathy-qt (0.9.7-4) unstable; urgency=medium

  * Team upload.
  * Explicitly add build dependencies whose stuff is explicitly searched by
    the build system: libdbus-1-dev, libdbus-glib-1-dev, libglib2.0-dev, and
    libxml2-dev. (Closes: #894154)
  * Remove the pkg-config dependency in libtelepathy-qt5-dev: the majority of
    the reverse dependencies use cmake and its config files, and the rest has
    pkg-config already as build dependency.
  * Drop patch fix_qt4_test_build.patch, since it is useful for the Qt4 build
    which is not done anymore.

 -- Pino Toscano <pino@debian.org>  Tue, 27 Mar 2018 22:55:29 +0200

telepathy-qt (0.9.7-3) unstable; urgency=medium

  * Team upload.
  * Properly handle the switch from symlink to directory for all the doc
    directories: (Closes: #886943)
    - add .maintscript snippets for all the binaries, invoking symlink_to_dir
    - add Pre-Depends: ${misc:Pre-Depends} for libtelepathy-qt5-dev (already
      in other binaries)
  * Switch Vcs-* fields to salsa.debian.org.
  * Bump Standards-Version to 4.1.3, no changes required.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
  * Switch from dh_install to dh_missing for --fail-missing.
  * Minor touches to copyright: use https for Format, and switch from
    Upstream-Source to Source.

 -- Pino Toscano <pino@debian.org>  Sat, 17 Mar 2018 17:31:40 +0100

telepathy-qt (0.9.7-2) unstable; urgency=medium

  * Team upload.
  * Update symbols files from buildd's logs.
  * Remove debian/gbp.conf file, since the packaging repository is not managed
    using git-buildpackage
    - drop note about it in README.source
  * Stop suggesting libtelepathy-qt5-doc in libtelepathy-qt5-dev, since that
    -doc package does not exist.
  * Remove the Qt4 packages, as they are not used in the archive anymore:
    (Closes: #875206)
    - remove the libtelepathy-qt4-2, libtelepathy-qt4-farstream2,
      libtelepathy-qt4-dev, and libtelepathy-qt4-doc packages (Closes: #801817)
    - remove the libqt4-dev, qt4-dev-tools, and qt4-doc-html build dependencies
    - remove the double build in rules
    - remove also the doxygen, and graphviz build dependencies, since there is
      no documentation generated anymore
  * Update Vcs-* fields.

 -- Pino Toscano <pino@debian.org>  Mon, 25 Dec 2017 00:47:45 +0100

telepathy-qt (0.9.7-1) unstable; urgency=medium

  * Team upload.

  [ Rohan Garg ]
  * New upstream release
  * Drop upstreamed patches
  * Refresh improve-doxygen-build.patch
  * Add fix_qt4_test_build.patch
  * Bump standards version, no changes required

  [ Pino Toscano ]
  * Update symbols files.
  * Drop libssl build dependency, since it does not seem to be needed, and
    it is not clear why it was added in the first place. (Closes: #882562)
  * Update Vcs-Browser field.
  * Remove libtelepathy-qt4-dbg, and libtelepathy-qt5-dbg in favour of the
    -dbgsym packages.
  * Remove trailing whitespaces in changelog.
  * Simplify watch file, and switch it to https.
  * Drop outdated dpkg-dev build dependency.
  * Bump Standards-Version to 4.1.2, no changes required.
  * Remove the get-orig-source target in rules, since plain uscan works fine.
  * Fix the version of the older symbols of libtelepathy-qt5-0, and
    libtelepathy-qt5-farstream0: 0.0.9.5 was surely a typo for 0.9.5.
  * Remove the symlinking of the doc directories to the one of
    libtelepathy-qt4-2, otherwise it makes everything depend on Qt4 (for
    little to no gain).

 -- Pino Toscano <pino@debian.org>  Sat, 09 Dec 2017 08:27:03 +0100

telepathy-qt (0.9.6.1-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add fix-failing-test.patch to fix failing tests on some architectures
    (Closes: #859462)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Wed, 03 May 2017 22:56:53 +0200

telepathy-qt (0.9.6.1-6) unstable; urgency=medium

  [ Diane Trout ]
  * Add fix-deprecated-bsd-source.patch
  * Update force-service-pic.patch to list some necessary dependencies
    (Closes: #819237)

  [ Rohan Garg ]
  * Add fix-failing-test.patch to fix some failing tests

 -- Diane Trout <diane@debian.org>  Fri, 25 Mar 2016 13:56:45 -0700

telepathy-qt (0.9.6.1-5) unstable; urgency=medium

  * Add force-service-pic.patch to allow linking against service library
    from shared libraries. (Closes #80187)
  * Enable python-dbus and add libxml2-utils to build-deps, because tests
    are good.
  * Update vcs urls to use secure urls.

 -- Diane Trout <diane@debian.org>  Thu, 28 Jan 2016 21:14:52 -0800

telepathy-qt (0.9.6.1-4) unstable; urgency=medium

  * Update libssl depenency to libssl1.0.2 (Closes: #805555)

 -- Diane Trout <diane@debian.org>  Wed, 02 Dec 2015 11:42:19 -0800

telepathy-qt (0.9.6.1-3) unstable; urgency=medium

  * Add gstreamer-1.5.patch (Closes: #800629)
    (Works around changes to location of gstreamer)

 -- Diane Trout <diane@debian.org>  Tue, 06 Oct 2015 16:07:14 -0700

telepathy-qt (0.9.6.1-2) unstable; urgency=medium

  * Update symbols files with 0.9.6.1-1 build logs

 -- Diane Trout <diane@debian.org>  Wed, 26 Aug 2015 14:54:58 -0700

telepathy-qt (0.9.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Update symbols files.
  * Update Diane Trout's email address.

 -- Diane Trout <diane@debian.org>  Fri, 03 Jul 2015 15:36:45 -0700

telepathy-qt (0.9.5+dfsg-6) unstable; urgency=medium

  * Update both symbols files.
    - tagged symbols in qt5 as optional=gccinternal
    - changed a couple of symbols in qt4 as gccinternal instead
      of inline

 -- Diane Trout <diane@ghic.org>  Sun, 24 May 2015 23:32:58 -0700

telepathy-qt (0.9.5+dfsg-5) unstable; urgency=medium

  * Update libtelepathy-qt4-2.symbols
    - Tag a few armel symbols as being optional.

 -- Diane Trout <diane@ghic.org>  Sat, 23 May 2015 19:15:49 -0700

telepathy-qt (0.9.5+dfsg-4) unstable; urgency=medium

  * Update telepathy symbols files using the build logs.

 -- Diane Trout <diane@ghic.org>  Thu, 21 May 2015 16:59:13 -0700

telepathy-qt (0.9.5+dfsg-3) unstable; urgency=medium

  * Remove libssl1.0.0 version dependency (Closes: #784300)
  * Add myself and Michał Zając to the debian/copyright block
  * Update libtelepathy-qt5-0.symbols
  * Disable python-dbus, python-gobject build-depends as they are
    for disabled tests.
  * Move uscan --repack command out of separate script and into
    debian/rules

 -- Diane Trout <diane@ghic.org>  Thu, 07 May 2015 13:12:05 -0700

telepathy-qt (0.9.5+dfsg-2) unstable; urgency=medium

  * Updates that should have been included in the 0.9.5+dfsg-1 changelog.
    - The dfsg repack was triggered by the inclusion of a minified
      jquery library. Since I needed to repack, I stripped out
      the pre-built documentation.
    - I added a limitation against using libssl1.0.0 >= 1.0.2
      from experimental because of changed symbols
  * Update standards-version to 3.9.6, no changes needed.
  * Update symbols files using many architectures.
    - Removed several optional symbols that didn't appear on
      any architecture.
  * Release to unstable.

 -- Diane Trout <diane@ghic.org>  Sat, 25 Apr 2015 17:14:10 -0700

telepathy-qt (0.9.5+dfsg-1) experimental; urgency=medium

  * New upstream release.
  * Update build-depends to use gstreamer 1.0
  * Add libtelepathy-qt4-service.a to -dev.install,
    it's currently being distributed as a static library while
    the API is being stabilized.
  * Update libtelepathy-qt4-2.symbols file for new version.
  * Update libtelepathy-qt4-farstream2.symbols for new version.
  * Update libfarstream to 0.2 (>= 0.2.3)
  * Update libtelepathy-farstream-dev build-depends to 0.6
  * Adjust libtelepathy-qt4-dev.install paths to only install qt4 versions
  * Build qt5 version adding the following packages.
    - libtelepathy-qt5-0
    - libtelepathy-qt5-farstream0
    - libtelepathy-qt5-dev

 -- Diane Trout <diane@ghic.org>  Mon, 07 Jul 2014 22:36:28 -0700

telepathy-qt (0.9.4+dfsg-3) unstable; urgency=medium

  * Move building documentation to override_dh_auto_build-indep target.
  * Use dh_doxygen -i in override_dh_instaldocs so it only triesto clean
    documentation when building the indep package.

 -- Diane Trout <diane@ghic.org>  Tue, 24 Jun 2014 20:38:12 -0700

telepathy-qt (0.9.4+dfsg-2) unstable; urgency=medium

  * Add update-doxygen.patch to remove deprecated tags.
  * replace dh_doxygen with directly searching for doxygen .map and .md5s
    The dh_doxygen version wasn't finding the unneeded files and was
    causing the build to fail.

 -- Diane Trout <diane@ghic.org>  Sun, 22 Jun 2014 22:54:13 -0700

telepathy-qt (0.9.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Verify upstream pgp signature.
  * Remove fix_storing_avatars.patch. Applied upstream.
  * Remove create_moc_command.patch. Applied upstream.
  * Remove link-against-gobject.patch. Seems unnecessary.
  * Update libtelepathy-qt4-2 symbols file.
  * Add get-orig-source target to repack upstream to remove minified jquery.js
  * Add improve-doxygen-build.patch to fix issues with doxygen build process
  * Add qt4-dev-tools and qt4-doc-html to build-depends so we can
    rebuild documentation.
  * Install documentation from build directory.
  * Clean up doxygen documentation tree.
  * Specify location of local Qt documentation.

 -- Diane Trout <diane@ghic.org>  Thu, 19 Jun 2014 10:31:23 -0700

telepathy-qt (0.9.3-4) unstable; urgency=medium

  * Update symbols files for other architectures.

 -- Diane Trout <diane@ghic.org>  Tue, 03 Jun 2014 23:12:56 -0700

telepathy-qt (0.9.3-3) unstable; urgency=low

  * Remove version requirement from pkg-config dependency (Closes: #734489)
  * Update Standards-Version to 3.9.5. No Changes needed.
  * Update debian/libtelepathy-qt4-2.symbols for 0.9.3 symbols.
  * Explicitly use pkg-kde-tools for its symbol helper utilities.
  * Explicitly force build type RelWithDebInfo

 -- Diane Trout <diane@ghic.org>  Mon, 20 Jan 2014 14:47:29 -0800

telepathy-qt (0.9.3-2) unstable; urgency=low

  * Add fix_storing_avatars.patch. Fixes issue storing avatars
    multiple times.
  * Remove python compiled byte-code during debian auto_clean target.
  * Add create_moc_command.patch to work around a change in a recent
    cmake macro. (Closes: #728675)

 -- Diane Trout <diane@ghic.org>  Wed, 27 Nov 2013 08:49:05 -0800

telepathy-qt (0.9.3-1) unstable; urgency=low

  * Team upload.

  [ Diane Trout ]
  * New upstream release.
  * Bump libfarstream-0.1-dev, libtelepathy-farstream-dev,
         libtelepathy-glib-dev dependencies.
  * Remove fix_ftbfs_gcc47.patch applied upstream.
  * Update libtelepathy-qt4-2.symbols.
  * Remove git-buildpackage directory configuration options.
  * Remove uneeded shlib:Depends from libtelepathy-qt4-dev control .
  * Refresh patches.
  * Add patch link-against-gobject.patch to provide g_object symbols
    used by libtelepathy-qt4-farstream.
  * Add myself to Uploaders.
  * Update Vcs-* fields to canonical URIs.
  * Update Standards-Version: 3.9.4. No changes needed.
  * Add Multi-Arch: same to libtelepathy-qt4-dbg.

  [ Michał Zając ]
  * Drop fvisibility-inlines-hidden.patch - went upstream
  * Change the Maintainer, ACKed by Simon McVittie:
    - Change the Maintainers field to the KDE extras team.
    - Update the Uploaders.
  * Update symbols file
  * Updated Vcs-* fields

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Remove upstream stuff from the repo. We are going to keep only debian/ on
    it.
  * Update symbols file with current amd64 build.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 05 Jul 2013 17:48:44 -0300

telepathy-qt (0.9.1-4) unstable; urgency=low

  * Add extra (optional=inline) markings on all inline
    symbols that still appear in the symbols file and confirm
    it with pkgkde-symbolshelper for all architectures. (Closes: #676259)

 -- George Kiagiadakis <kiagiadakis.george@gmail.com>  Sun, 17 Jun 2012 11:08:30 +0200

telepathy-qt (0.9.1-3) unstable; urgency=low

  * Add patch fvisibility-inlines-hidden.patch to pass
    -fvisibility-inlines-hidden to the compiler and avoid
    having inline functions in the symbols file.
  * Cleanup libtelepathy-qt4-2.symbols from all inline
    functions to fix FTBFS (Closes: #676259).

 -- George Kiagiadakis <kiagiadakis.george@gmail.com>  Thu, 07 Jun 2012 00:20:45 +0200

telepathy-qt (0.9.1-2) unstable; urgency=low

  * Team upload.
  * Fix FTBFS due to jquery symlink creation

 -- Laurent Bigonville <bigon@debian.org>  Tue, 05 Jun 2012 12:47:15 +0200

telepathy-qt (0.9.1-1) unstable; urgency=low

  * Team upload.
  * New upstream release. (Closes: #647835)
    - Upstream source package renamed to telepathy-qt

  [ Laurent Bigonville ]
  * Build against farstream instead of farsight (Closes: #672107)
  * Bump soname of both libraries
  * Bump build-dependencies
  * Switch to dpkg-source 3.0 (quilt) format
  * debian/control: Bump Standards-Version to 3.9.3 (no further changes)
  * Bump debhelper compatibility to 9
  * debian/watch: Update watch file to match new upstream name
  * Link jquery.js file to the one provided by libjs-jquery

  [ George Kiagiadakis ]
  * Change the gcc-4.7 ftbfs patch to use the upstream patch.
  * Switch build system to dh.
  * Convert to multiarch.
  * Install the TelepathyQt4*Config.cmake and related files.
  * Add symbols files.
  * Update README.source.
  * Update debian/copyright.

 -- Laurent Bigonville <bigon@debian.org>  Mon, 04 Jun 2012 14:33:00 +0200

telepathy-qt4 (0.7.1-1.1) unstable; urgency=low

  * Non-maintainer upload.

  [ Cyril Brulebois ]
  * Fix FTBFS with gcc 4.7 by fixing missing <unistd.h> includes
    (Closes: #667391).

 -- gregor herrmann <gregoa@debian.org>  Mon, 28 May 2012 16:21:33 +0200

telepathy-qt4 (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Update dependency version requirements.
  * debian/rules: Add code to bump makeshlibs args version automatically.
  * Bump standards-version to 3.9.2; no changes required.
  * Add myself to uploaders.

 -- George Kiagiadakis <kiagiadakis.george@gmail.com>  Sat, 11 Jun 2011 14:47:18 +0300

telepathy-qt4 (0.5.15-1) experimental; urgency=low

  * New upstream release.
  * debian/rules: Update to use cmake now.
  * debian/control: Update Build-Deps for new version.
  * Bump package name sonames for new library soname.
  * debian/copyright: Update copyright years.

 -- Jonny Lamb <jonny@debian.org>  Thu, 14 Apr 2011 11:33:54 +0100

telepathy-qt4 (0.3.8-1) unstable; urgency=low

  * New upstream relase.
  * Make libtelepathy-qt4-dev depend on libtelepathy-farsight-dev,
    to agree with the header and runtime dependencies on the same.
  * Add myself to Uploaders.

 -- Adam Conrad <adconrad@0c3.net>  Wed, 25 Aug 2010 16:31:01 -0600

telepathy-qt4 (0.3.6-1) unstable; urgency=low

  [ George Goldberg ]
  * Update to new upstream release

 -- Jonny Lamb <jonny@debian.org>  Wed, 14 Jul 2010 14:18:54 +0100

telepathy-qt4 (0.3.5-1) unstable; urgency=low

  [ George Goldberg ]
  * Update to new upstream release

  [ Jonny Lamb ]
  * debian/control: Upped Standards-Version. (no changes)

 -- Jonny Lamb <jonny@debian.org>  Thu, 24 Jun 2010 18:57:42 +0100

telepathy-qt4 (0.3.2-1) unstable; urgency=low

  [ George Goldberg ]
  * Update to new upstream release
  * Build now depends on telepathy-glib >=0.10.0

  [ Jonny Lamb ]
  * debian/control: Added George and myself to Uploaders.

 -- Jonny Lamb <jonny@debian.org>  Sat, 24 Apr 2010 14:47:07 +0100

telepathy-qt4 (0.2.0-2) unstable; urgency=low

  * Move Build-Depends-Indep to Build-Depends, working around #478524 in
    sbuild (Closes: #557032)
  * Disable tests for now, due to race conditions in the test code that can't
    usefully be debugged on a buildd

 -- Simon McVittie <smcv@debian.org>  Thu, 19 Nov 2009 12:22:40 +0000

telepathy-qt4 (0.2.0-1) unstable; urgency=low

  * Initial Debian packaging (Closes: #539008)

 -- Simon McVittie <smcv@debian.org>  Tue, 10 Nov 2009 14:49:41 +0000
